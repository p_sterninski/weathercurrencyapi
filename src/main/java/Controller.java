

public class Controller {
	
	public Controller() {
		
		View view = new View();
		
		view.addShowWeatherListener(e -> {
			Service service = new Service(view.getTfCountry());
			String weatherDescription = service.getWeatherDescription(view.getTfCity());
			if(weatherDescription != "") {
				view.setTaWeatherArea(weatherDescription);
			} else {
				view.setTaWeatherArea("N/A");
			}
		});
		

		view.addShowRateForListener(e -> {
			Service service = new Service(view.getTfCountry());
			String baseCurrency = view.getTfCurrencySymbol();
			String targetCurrency = service.getCountryCurrency();
			Double rate = service.getRateFor(baseCurrency);
			if(rate != -1.0) {
				view.setTfRateFor(String.format("%.4f", rate) + " " + targetCurrency.toUpperCase() + "/" + baseCurrency.toUpperCase());
			} else {
				view.setTfRateFor("N/A");
			}
		});

		
		view.addShowNBPRateListener(e -> {
			Service service = new Service(view.getTfCountry());
			String baseCurrency = service.getCountryCurrency();
			String targetCurrency = "PLN";
			Double rate = service.getNBPRate();
			if(rate != -1.0) {
				view.setTfNBPRate(String.format("%.4f", rate) + " " + targetCurrency.toUpperCase() + "/" + baseCurrency.toUpperCase());
			} else {
				view.setTfNBPRate("N/A");
			}
		});
		
		
		view.addShowCityWebSite( e -> {
			Service service = new Service(view.getTfCountry());
			String cityWebSiteURL = service.getCityWebSiteURL(view.getTfCity());
			view.openCityWebSite(cityWebSiteURL);
		});
		
	}
}
