

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class View extends JFrame {
	
	private JButton bShowWeather = new JButton("Show Weather");
	private JButton bShowRateFor = new JButton("Show Rate");
	private JButton bShowNBPRate = new JButton("Show NBP Rate");
	private JButton bShowCityWebSite = new JButton("Show City Web Site");
	private JTextField tfCountry = new JTextField(20);
	private JTextField tfCity = new JTextField(20);
	private JTextArea taWeather = new JTextArea(6, 20);
	private JTextField tfCurrencySymbol = new JTextField(5);
	private JTextField tfRateFor = new JTextField(10);
	private JTextField tfNBPRate = new JTextField(10);
	
	
	public View() {
		Container cp = this.getContentPane();
		cp.setLayout(new BoxLayout(cp, BoxLayout.Y_AXIS));
		
		cp.add(createLabelComponentPanel("Type country name: ", tfCountry));
		cp.add(Box.createRigidArea(new Dimension(0, 10)));
		cp.add(createLabelComponentPanel("Type city name: ", tfCity));
		cp.add(Box.createRigidArea(new Dimension(0, 40)));
		cp.add(createWeatherPanel());
		cp.add(Box.createRigidArea(new Dimension(0, 40)));
		cp.add(createRateForPanel());
		cp.add(Box.createRigidArea(new Dimension(0, 40)));
		cp.add(createNBPRatePanel());
		cp.add(Box.createRigidArea(new Dimension(0, 40)));
		cp.add(createCityWebSitePanel());
		
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    this.pack();
	    this.setVisible(true);
	}


	public String getTaWeatherArea() {
		return taWeather.getText();
	}


	public void setTaWeatherArea(String weatherDescription) {
		this.taWeather.setText(weatherDescription);
	}


	public String getTfRateFor() {
		return tfRateFor.getText();
	}


	public void setTfRateFor(String rateValue) {
		this.tfRateFor.setText(rateValue);;
	}


	public String getTfNBPRate() {
		return tfNBPRate.getText();
	}


	public void setTfNBPRate(String rateValue) {
		this.tfNBPRate.setText(rateValue);
	}
	

	public String getTfCountry() {
		return tfCountry.getText();
	}


	public void setTfCountry(String countryName) {
		this.tfCountry.setText(countryName);
	}


	public String getTfCity() {
		return tfCity.getText();
	}


	public void setTfCity(String cityName) {
		this.tfCity.setText(cityName);
	}


	public String getTfCurrencySymbol() {
		return tfCurrencySymbol.getText();
	}


	public void setTfCurrencySymbol(String CurrencySymbol) {
		this.tfCurrencySymbol.setText(CurrencySymbol);
	}
	


	private JPanel createWeatherPanel() {
		JPanel pWeather = new JPanel();
		pWeather.setLayout(new BoxLayout(pWeather, BoxLayout.Y_AXIS));
		
		taWeather.setEditable(false);
		
		pWeather.add(createLabelComponentPanel("Output: ", taWeather));
		pWeather.add(Box.createRigidArea(new Dimension(0, 10)));
		pWeather.add(bShowWeather);
		
		pWeather.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder()
															, "Weather Forecast"
															, TitledBorder.CENTER
															, TitledBorder.DEFAULT_POSITION));
		
		
		pWeather.setMaximumSize(new Dimension(400, 300));
		pWeather.setMinimumSize(new Dimension(400, 300));
		return pWeather;
	}
	
	private JPanel createNBPRatePanel() {
		JPanel pRate = new JPanel();
		pRate.setLayout(new BoxLayout(pRate, BoxLayout.Y_AXIS));
		
		tfNBPRate.setEditable(false);
		pRate.add(createLabelComponentPanel("Output: ", tfNBPRate));
		
		pRate.add(Box.createRigidArea(new Dimension(0, 10)));
		pRate.add(bShowNBPRate);
		
		pRate.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder()
														, "NBP Rate"
														, TitledBorder.CENTER
														, TitledBorder.DEFAULT_POSITION));
		
		pRate.setMaximumSize(new Dimension(400, 300));
		pRate.setMinimumSize(new Dimension(400, 300));
		return pRate;
	}

	
	private JPanel createRateForPanel() {
		JPanel pRate = new JPanel();
		pRate.setLayout(new BoxLayout(pRate, BoxLayout.Y_AXIS));
		
		tfRateFor.setEditable(false);

		pRate.add(createLabelComponentPanel("Type currency symbol: ", tfCurrencySymbol));
		pRate.add(Box.createRigidArea(new Dimension(0, 10)));
		pRate.add(createLabelComponentPanel("Output: ", tfRateFor));
		pRate.add(Box.createRigidArea(new Dimension(0, 10)));
		pRate.add(bShowRateFor);
		
		pRate.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder()
														, "Rate for given currency"
														, TitledBorder.CENTER
														, TitledBorder.DEFAULT_POSITION));
		
		pRate.setMaximumSize(new Dimension(400, 300));
		pRate.setMinimumSize(new Dimension(400, 300));
		return pRate;
	}
	
	
	private JPanel createCityWebSitePanel() {
		JPanel pCityWebSite = new JPanel();
		pCityWebSite.setLayout(new BoxLayout(pCityWebSite, BoxLayout.Y_AXIS));
		
		pCityWebSite.add(bShowCityWebSite);
		
		pCityWebSite.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder()
																, "City Web Site"
																, TitledBorder.CENTER
																, TitledBorder.DEFAULT_POSITION));		
		
		pCityWebSite.setMaximumSize(new Dimension(400, 300));
		pCityWebSite.setMinimumSize(new Dimension(400, 300));
		return pCityWebSite;
	}
	
	
	private JPanel createLabelComponentPanel(String labelText, JComponent component) {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setAlignmentX(LEFT_ALIGNMENT);
		panel.setMaximumSize(new Dimension(400, 300));
		panel.setMinimumSize(new Dimension(400, 300));
		
		panel.add(new JLabel(labelText));
		panel.add(component);
		return panel;
	}
	
	
	public void openCityWebSite(String cityWebSiteUrl) {
		SwingUtilities.invokeLater(new Runnable() {
			
        	@Override
            public void run() {
				JFXPanel fxPanel = new JFXPanel();
				
				Platform.runLater(new Runnable() {
		            
		        	@Override
		            public void run() {
						WebView webView = new WebView();
						WebEngine webEngine = webView.getEngine();
						webEngine.load(cityWebSiteUrl);
						fxPanel.setScene(new Scene(webView));
		        	}
				});
				
				JFrame window = new JFrame();
				window.add(fxPanel);
				window.setSize(800, 600);
				window.setVisible(true);

        	}
		});

	}
	
	
	
	public void addShowWeatherListener(ActionListener action) {
		bShowWeather.addActionListener(action);
	}
	
	
	public void addShowRateForListener(ActionListener action) {
		bShowRateFor.addActionListener(action);
	}

	
	public void addShowNBPRateListener(ActionListener action) {
		bShowNBPRate.addActionListener(action);
	}
	
	
	public void addShowCityWebSite(ActionListener action) {
		bShowCityWebSite.addActionListener(action);
	}
}
