

import java.util.List;

public class ExchangeRateNBP {
	String table;
	String currency;
	String code;
	List<RateInfo> rates;
	
	class RateInfo {
		String no;
		String effectiveDate;
		Double mid;
	}

}
