

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Currency;
import java.util.Locale;

import com.google.gson.Gson;

public class Service {
	
	private String countryName = "";
	private String countryCode = "";
	private String countryCurrency = "";
	
	
	
	public Service(String country) {
		Locale defaultLocale = Locale.ENGLISH;
		
		if(country.equals("")) {
			return;
		}
		
		//look for country in english and polish language (default is english)
		setCountryName(country);
		for (Locale locale : Locale.getAvailableLocales()) {
			if(locale.getDisplayCountry(defaultLocale).toUpperCase().equals(country.toUpperCase())) {
				setCountryName(locale.getDisplayCountry(defaultLocale));
				setCountryCode(locale.getCountry());	
				setCountryCurrency(Currency.getInstance(locale).getCurrencyCode());
				return;
			} else if(locale.getDisplayCountry(new Locale("pl")).toUpperCase().equals(country.toUpperCase())) {
				setCountryName(locale.getDisplayCountry(defaultLocale));
				setCountryCode(locale.getCountry());	
				setCountryCurrency(Currency.getInstance(locale).getCurrencyCode());
				return;
			}
		}
	}
	
	
	
	public String getWeather(String city) {
		String UrlPath = "http://api.openweathermap.org/data/2.5/weather?q=" + city + ",country=" + getCountryCode() + "&mode=json&appid=301b9f3c04df25e394424ebe1a58f821";
		String weatherForecast = "";
		
		try {
			if(!city.equals("")) {
				weatherForecast = getApiContent(UrlPath); //weather information in JSON format
			}
		} catch (Exception e) {
			weatherForecast = "";
		}
		
		return weatherForecast; 
	}
	
	
	
	public String getWeatherDescription(String city) {
		String weatherDescription = ""; 
		Gson gson = new Gson();
		Weather weather = null;
		try {
			weather = gson.fromJson(getWeather(city), Weather.class);
			
			weatherDescription = "main: \t" + weather.weather.get(0).main
								+ "\ndescription: \t" + weather.weather.get(0).description
								+ "\ntemp: \t" + weather.main.get("temp")
								+ "\npressure: \t" + weather.main.get("pressure")
								+ "\nhumidity: \t" + weather.main.get("humidity")
								+ "\nspeed of wind: \t" + weather.wind.get("speed");
		
		} catch (Exception e) {
			weatherDescription = "";
		}
		
		return weatherDescription;
	}
	
	
	
	public Double getRateFor(String currency) {
		String UrlPath = "http://data.fixer.io/api/latest?access_key=9ca8306581a5fdb7184352fefa9c0546&symbols=" + getCountryCurrency() + "," + currency;
		String jsonContent = "";
		Gson gson = new Gson();
		ExchangeRateFixer exchangeRate = null;
		Double rate = -1.0;
		try {
			jsonContent = getApiContent(UrlPath);
			exchangeRate = gson.fromJson(jsonContent, ExchangeRateFixer.class);
			rate = exchangeRate.rates.get(getCountryCurrency().toUpperCase()) / exchangeRate.rates.get(currency.toUpperCase());
		} catch (Exception e) {
			rate = -1.0;
		}
		
		return rate;
	}
	
	
	
	public Double getNBPRate() {
		Double rate = -1.0;
		if(getCountryCurrency().equals("PLN")) {
			rate = 1.0000;
		} else {
			
			String UrlPathA = "http://api.nbp.pl/api/exchangerates/rates/a/" + getCountryCurrency() + "/?format=json";
			String UrlPathB = "http://api.nbp.pl/api/exchangerates/rates/b/" + getCountryCurrency() + "/?format=json";
			String jsonContent = "";
			Gson gson = new Gson();
			ExchangeRateNBP exchangeRate = null;
			try {
				jsonContent = getApiContent(UrlPathA);
				exchangeRate = gson.fromJson(jsonContent, ExchangeRateNBP.class);		
				rate = Double.valueOf(exchangeRate.rates.get(0).mid);
			} catch(Exception e) {
				try {
					jsonContent = getApiContent(UrlPathB);
					exchangeRate = gson.fromJson(jsonContent, ExchangeRateNBP.class);		
					rate = Double.valueOf(exchangeRate.rates.get(0).mid);
				} catch (Exception e1) {
					rate = -1.0;
				}
			}
		}
		
		
		return rate;		
	}
	
	
	
	private String getApiContent(String URLPath) throws Exception{
		StringBuffer apiContent = new StringBuffer("");
		
		URL url = new URL(URLPath);
		try(
			BufferedReader connection = new BufferedReader(
											new InputStreamReader(
													url.openConnection().getInputStream()))
		){
			String line;
			while((line = connection.readLine()) != null) {
				apiContent.append(line);
			}
		
		}
		
		return apiContent.toString();
	}
	
	
	
	public String getCityWebSiteURL(String city) {
		String url = "https://en.wikipedia.org/wiki/" + city;
		
		return url;
	}
	
	
//countryName
	public String getCountryName() {
		return countryName;
	}


	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	
//CountryCode
	public String getCountryCode() {
		return countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	
//countryCurrency
	public void setCountryCurrency(String countryCurrency) {
		this.countryCurrency = countryCurrency;
	}
	
	public String getCountryCurrency() {
		return countryCurrency;
	}
	
}  

