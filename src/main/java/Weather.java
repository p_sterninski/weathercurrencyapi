

import java.util.List;
import java.util.Map;

public class Weather {
	Map<String, String> coord;
	List<WeatherShortInfo> weather;
	String base;
	Map<String, String> main;
	String visibility;
	Map<String, String> wind;
	Map<String, String> clouds;
	String dt;
	Map<String, String> sys;
	String id;
	String name;
	String cod;
	
	
	class WeatherShortInfo {
		String id;
		String main;
		String description;
		String icon;
	}
}
